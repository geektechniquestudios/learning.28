package com.geektechnique.dockerpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerPracticeApplication.class, args);
	}

}

//docker -f is for the name of dockerfile, -t is for tag
//docker build -f Dockerfile -t name-of-jar .

//docker run -p port-outside:port-inside-docker name-of-dockerfile
//docker run -p 8081:8080 docker-practice

//-d = daemon;
//sudo docker run -d -p 8080:8080 -p 80:80 -v $PWD/traefik.toml:/etc/traefik/traefik.toml -v /var/run/docker.sock:/var/run/docker.sock traefik
