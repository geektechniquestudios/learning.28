package com.geektechnique.dockerpractice;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicController {

    @RequestMapping("/")
    public String sayHello(){
        return "Hello";
    }
}
