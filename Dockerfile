FROM openjdk:8
ADD target/docker-practice-0.0.1-SNAPSHOT.jar docker-practice-0.0.1-SNAPSHOT.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar","docker-practice-0.0.1-SNAPSHOT.jar"]